import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  form = {}

  constructor(public toastController: ToastController) { }

  submit(personForm: NgForm) {
    if (personForm.invalid) {
      return
    }
    console.log(this.form)
    this.presentToast()
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Form submitted.',
      duration: 5000
    });
    toast.present();
  }
}
